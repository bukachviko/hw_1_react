import {useState} from "react";
import './App.css';
import Modal from "./Components/Modal";
import Button from "./Components/Button";

function App() {

    const [isShowModal, setIsShowModal] = useState(false);
    const [modalType, setModalType] = useState(1);



const openRedWindow = () => {
    setModalType(1)
    toggleModal()
}

const openBlueWindow = () => {
    setModalType(2)
    toggleModal()
}

    const toggleModal = () => {
        setIsShowModal(!isShowModal)
    }

    return (
        <div className="App">
            <Button
                key={1}
                btnClass="openFirstModalBtn"
                backgroundColor={{backgroundColor: '#f53e3e'}}
                text={"Open first modal"}
                myClick={openRedWindow}
            />

            <Button
                key={2}
                btnClass="openSecondModalBtn"
                backgroundColor={{backgroundColor: '#3eaff5'}}
                text={"Open second modal"}
                myClick={openBlueWindow}
            />

            {isShowModal && modalType === 1 && <Modal key={3}
               backgroundColor={{backgroundColor: '#f53e3e'}}
               header={"Do you want to delete this file?"}
               text={"Once you delete this file, it won`t be possible to undo this action.Are you sure you want delete it?"}
               boolean={true}
               nameBtn1={'ok'}
               nameBtn2={'Cancel'}
               toggleModal={toggleModal}
            />}

            {isShowModal && modalType === 2 && < Modal key={4}
               backgroundColor={{backgroundColor: '#3eaff5'}}
               header={"Ти отримав сьогодні подарунок?"}
               text={"Вітаю з Днем Святого Миколая і бажаю приємних сюрпризів, чарівних чудес і оптимізму душі, доброго здоров’я і приголомшливих ідей, люблячої родини і справжнього щастя."}
               boolean={true}
               nameBtn1={'Ні!'}
               nameBtn2={'Дякую!'}
               toggleModal={toggleModal}
            />}
        </div>
    );
}

export default App;
