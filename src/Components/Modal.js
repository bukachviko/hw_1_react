import React from 'react';
import "./Modal.css";
import Button from "./Button";

export default function Modal({backgroundColor, header, text: mainText, nameBtn1, nameBtn2, toggleModal}) {

    return (

        <div className="modal">
            <div onClick={toggleModal}
                 className="overlay"></div>
            <div className="modal-content" style={backgroundColor}>
                <div className="header">
                    <h1 className="header__title">{header}</h1>
                    <Button
                        btnClass="headerCloseBtn"
                        myClick={toggleModal}
                        text={'Х'}
                    />
                </div>
                <p className="text">{mainText}</p>
                <div className="footer">
                    <Button
                        text={nameBtn1}
                        myClick={toggleModal}
                    />

                    <Button
                        text={nameBtn2}
                        myClick={toggleModal}
                    />
                </div>
            </div>
        </div>
    );
}

